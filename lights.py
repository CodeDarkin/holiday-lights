import board
import neopixel
import time
from random import randint

#holiday tree lights by codedarkin

pixels = neopixel.NeoPixel(board.D18, 240, auto_write=False)

#turn all lights one color
def one_color(tup):
  for i in range(0, 240):
    pixels[i] = tup

#turn off all lights
def off():
  one_color((0,0,0))

#create gold coloring at top of tree
def goldcap():
  for i in range(120, 130):
    pixels[i] = (255,215,0)

#default pause time
def sleep():
  time.sleep(.35)


while True:
  try:
#make all green
    one_color((0,128,0))
#make some random pixels red
    for i in range(0,240,randint(10,15)):
      pixels[i] = (255,0,0)
    goldcap()
    pixels.show()
    sleep()
    for i in range(0,240,10):
      random = randint(0,10)
#turn some pixels off
      if(random<5):
        pixels[i] = (0,0,0)
      if(random>5):
#turn some pixels white
        pixels[i] = (255,255,255)
    goldcap()
    pixels.show()
    sleep()

  except KeyboardInterrupt:
    off()
    pixels.show()
    raise
