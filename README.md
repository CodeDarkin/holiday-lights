# Holiday Lights

## Holiday lights using Neopixel lights and Python script

Enjoy this simple DIY for your LED lights during the holidays!

[Hardware Schematics](https://www.thegeekpub.com/15990/wiring-ws2812b-addressable-leds-to-the-raspbery-pi/)

## Software Requirements
`sudo pip3 install rpi_ws281x adafruit-circuitpython-neopixel`

### If you would like the lights to turn on whenever the Raspberry Pi is powered on
`sudo nano /etc/profile`

Then add the command to run the Python script at the bottom of the file. Be sure to change the path to your script if needed

`sudo python3 ~/lights.py`
